# Trabajo Final
Repositorio público de los trabajos finales que llevan a cabo lxs alumnxs de la Tecnicatura Universitaria en Software Libre de la Facultad de Ing. y Cs. Hídricas de la Universidad Nacional del Litoral (Santa Fe, Argentina)

Docente: Emiliano López

Docente: Cristian Quinteros (2024)

## Proyectos 2020

## Proyectos 2019

- https://gitlab.com/milton89/administracion-centralizada - MILTON LAUTARO HUGUENET (Administración)
- https://gitlab.com/zapaf/tp_final_tusl - FRANCISCO ZAPATA (Administración)
- https://gitlab.com/fgenaro/implementacion-koha-para-diferentes-entornos - FRANCO GENARO (Administracion)
- https://gitlab.com/delforcastro/altemoi - DELFOR HERNÁN CASTRO (Desarrollo)
- https://gitlab.com/sebelk/yapokweb - SERGIO JAVIER BELKIN y ANDRES PERDIGUERO (Administración y Educación)

## Proyectos 2018

- [Gestion Hospitalaria usando GNU Health](https://gitlab.com/picober/GestionHospitalaria.git) - Miguel Arturo Bernechea
- [Gestión Cooperativa con Tryton ERP](https://gitlab.com/Hertux/Trabajo-Final) - Hernán Albornoz
- [Estructuras Básicas para Administradores de Sistemas Novatos](https://gitlab.com/colonista/guia-administradores-sistemas) - Lucas Sebastian Villa
- [Migración y Administración de red de Oficina al S.O. Xubuntu GNU/Linux](https://gitlab.com/fabian_dn/Trabajo-Final.git) - Carlos Fabián Delgado
- [Migración a Software Libre en el Centro Educativo UNL Virtual](https://gitlab.com/EmanuelBlanco/TrabajoFinal.git) - Emanuel Blanco
- [Vivas, libres y federadas. Herramientas para la comunicación de colectivas transfeministas en el Fediverso](https://gitlab.com/bicivoladora/tusl-trabajo-final) - Cecilia Ortmann
- [Entorno de programación LOGO para la plataforma AppInventor](https://gitlab.com/gdeldago/Manuelita) - Gustavo Del Dago
- [Mapa georreferencial Umap en servidor local para el Programa Munigestión](https://gitlab.com/emanueladriancastillo/Trabajo-Final-Tec.SL) - Emanuel Castillo
- [Stegopy: una aplicación esteganográfica](https://gitlab.com/andresbebe/Stegopy) - Alejandro Luis Bonavita, Andres Barroso
- [Detector optico de huevos fisurados](https://gitlab.com/mikiangel10/detector-huevos-fisurados) - Miguel Ángel Gómez
